cmake_minimum_required(VERSION 2.8.12)

MACRO(SUBDIRLIST result curdir)
	FILE(GLOB children RELATIVE ${curdir} ${curdir}/*)
	SET(dirlist "")
	FOREACH(child ${children})
	IF(IS_DIRECTORY "${curdir}/${child}")
		IF(EXISTS "${curdir}/${child}/CMakeLists.txt")
			LIST(APPEND dirlist ${child})
		ENDIF()
	ENDIF()
	ENDFOREACH()
	SET(${result} ${dirlist})
ENDMACRO()

get_filename_component(PROJECT_NAME ${CMAKE_CURRENT_SOURCE_DIR} NAME)
project(${PROJECT_NAME})

MACRO(GETPART url path)
	IF(IS_DIRECTORY "${CMAKE_SOURCE_DIR}/.git")
		execute_process(COMMAND git submodule add ${url} ${path} WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})
		execute_process(COMMAND git submodule update --recursive --init ${path} WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})
	ELSE()
		execute_process(COMMAND git clone --depth 1 --recursive ${url} ${path} WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})
	ENDIF()
ENDMACRO()

MACRO(GETPARTB url path barnch)
	IF(IS_DIRECTORY "${CMAKE_SOURCE_DIR}/.git")
		execute_process(COMMAND git submodule add --branch ${barnch} ${url} ${path} WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})
		execute_process(COMMAND git submodule update --recursive --init ${path} WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})
	ELSE()
		execute_process(COMMAND git clone --depth 1 --recursive ${url} --branch ${barnch} ${path} WORKING_DIRECTORY ${CMAKE_SOURCE_DIR})
	ENDIF()
ENDMACRO()

GETPART(https://gitlab.com/SR_team/SRSignal third-party/SRSignal)
GETPART(https://gitlab.com/SR_team/llmo third-party/llmo)
if(DXHOOK)
	GETPART(https://gitlab.com/prime-hack/samp/plugins/templates/ProxyDX9 third-party/ProxyDX9)
endif()
if(DRAWHOOK)
	GETPART(https://gitlab.com/prime-hack/samp/plugins/templates/DrawHook third-party/DrawHook)
endif()
if(DXHOOK OR DRAWHOOK)
	GETPART(https://gitlab.com/prime-hack/samp/plugins/templates/render third-party/render)
endif()
#GETPART(https://gitlab.com/SR_team/SRCursor third-party/SRCursor)
#GETPART(https://gitlab.com/SR_team/SRKeys third-party/SRKeys)
#GETPART(https://gitlab.com/SR_team/SREvents third-party/SREvents)
GETPART(https://gitlab.com/SR_team/SRDescent third-party/SRDescent)

include_directories(${CMAKE_CURRENT_SOURCE_DIR})

SUBDIRLIST(SUBDIRS ${CMAKE_CURRENT_SOURCE_DIR})
FOREACH(subdir ${SUBDIRS})
	include_directories(${CMAKE_CURRENT_SOURCE_DIR}/${subdir})
	ADD_SUBDIRECTORY(${subdir})
	message("Including third-party ${subdir}")
ENDFOREACH()
