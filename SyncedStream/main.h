#ifndef MAIN_H
#define MAIN_H

#include "loader/loader.h"
#include <SRDescent/SRDescent.h>
#include <Nop.h>
#include <Patch.h>

class AsiPlugin : public SRDescent {
	SRHook::Nop disableOverlappedIOFlag{ 0x406BD1, 5 };
	SRHook::Nop disableOverlappedIO{ 0x406BDB, 10 };
	SRHook::Patch skipOverlappedIOToggler{ 0x406C5D, { 0xEB } };

public:
	explicit AsiPlugin();
	virtual ~AsiPlugin();
};

#endif // MAIN_H
