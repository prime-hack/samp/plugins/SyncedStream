#include "main.h"

AsiPlugin::AsiPlugin() : SRDescent( nullptr ) {
	// Constructor
	disableOverlappedIOFlag.enable();
	disableOverlappedIO.enable();
	skipOverlappedIOToggler.enable();
}

AsiPlugin::~AsiPlugin() {
	// Destructor
	disableOverlappedIOFlag.disable();
	disableOverlappedIO.disable();
	skipOverlappedIOToggler.disable();
}
